module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                style: 'compressed'
            },
            dist: {
                files: {
                    'dist/css/<%=pkg.name%>.min.css': ['src/scss/boot.scss']
                }
            }
        },
        watch: {
            css: {
                files: ['src/scss/**/*.scss'],
                tasks: ['sass']
            }
        }
    });

    //grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-copy');
    //grunt.loadNpmTasks('grunt-contrib-uglify');

    //grunt.registerTask('default', ['concat', 'sass', 'uglify', 'watch']);
    //grunt.registerTask('default', ['concat', 'sass', 'copy', 'watch']);
    grunt.registerTask('default', ['sass', 'watch']);
};